console.log('Hello World? ');

//[SECTION] Arithmetic Operators

	let numA = 200;
	let numB = 6;
	//What we want is to get the sum of numA and numB;
	let sum = numA + numB;
	console.log(sum);

	// What we want is to get the difference of numA and numB;
	let difference = numA - numB;
	console.log (difference);

	let product = numA * numB;
	console.log(product);

	let quotient = numA / numB;
	console.log(quotient);

	// Remainder/Modulo
	let remainder = numA % numB;
	console.log(remainder);


// [SECTION] Assignment Operators
	// Basic Assignment Operator (=)
		// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

		let assignedNumber = 8;
		console.log(assignedNumber);

		assignedNumber = assignedNumber + 2
		console.log('Results of addition assignment operator: ' +assignedNumber);

		//Shorthand
		assignedNumber += 1;
		console.log('Result of addition assignment operator: ' +assignedNumber);

		//Subtraction/Multiplication/Division Assignment(-=,*=,/=)

		// Mini- Activity

		assignedNumber -= 1;
		console.log('Result of subtraction assignment operator: ' +assignedNumber);

				assignedNumber *= 1;
		console.log('Result of multiplication assignment operator: ' +assignedNumber);

				assignedNumber /= 1;
		console.log('Result of division assignment operator: ' +assignedNumber);


// [SECTION] Multiple Operators and Parentheses
		let mdas= 1 + 2 -3 *4 /5;
		console.log('Result of mdas operation: ' +mdas);

		/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule

		-The operations were done in the following order: 1 + 2 - ((3 * 4) / 5)

		*/

		let pemdas = 1 +(2-3) * (4/5);
		console.log('Results of Pemdas operation: ' + pemdas);

// [SECTION] Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increement/decrement was applied to

		//Pre-Increment
			let blankets = 10;  //Merong continuity
			let newBlankets = ++blankets;
			console.log('blankets: ' + blankets);
			console.log("newBlankets: " + newBlankets );
		//Post-Increment
			let pillows = 10; //Walang continuity
			let newPillows = pillows++;
			console.log('pillows: '+ pillows);
			console.log("newPillows: " + newPillows);

			//Mini-activity

			//Pre-Decrement
			let soda = 10;
			let newSoda = --soda;
			console.log('soda: ' + newSoda);
			console.log("newSoda: " + newSoda );

			//Post-decrement
			let pasta = 10;
			let newPasta = pasta--;
			console.log('pasta: ' + pasta);
			console.log("newPasta: " + newPasta );

// [ SECTION ] Comparison Operators
	
	let juan = 'juan';
	//equality operator (==)
	console.log(1 == 1);
	console.log(2 == 1);

	console.log(1 == '1');
	console.log (0 == false);

		console.log('juan' == 'juan');

		console.log( 'juan' == juan);

	//Inequality Operator (!=)
		console.log(1 != 1);
		console.log (2 != 1); 

		console.log (1 != '1');
		console.log(0 != false);

			console.log( 'juan' != "juan");
			console.log('juan' != juan);

	//Strict Equality Operator (===)

	console.log(1 === 1);
	console.log(1 === '1');
	console.log(0 === false);

	console.log('juan' === 'juan');
	console.log('juan' === juan);

	//Strict Inequality Operator(!==)
		console.log(1 !== 1);
		console.log(1 !== '1');
		console.log(0 !== false);

		console.log('juan' !== 'juan');
		console.log('juan' !== juan);

//  [SECTION] Relational Operators
	
	let a = 50;
	let b = 65;
	let c = 1;

	// Greater than (>)
		let isGreaterThan = a > b;
		console.log(isGreaterThan);

	// Less Than
		let isLessThan = a < b;
		console.log(isLessThan);

	//Greater than or equal to
	let isGtOrEqual = a >= b;
	console.log(isGtOrEqual);

	//Less than or equal to
	let isLtOrEqual = a <= b;
	console.log(isLtOrEqual);

		let numString = '30';
		console.log(a > numString); 

		console.log(b <= numString); 

		let string = 'twenty';
		console.log (b >= string);

// [SECTION] Logical Operators
	
	let isLegalAge = true;
	let isRegistered = false;

		//Logical AND Operator (&& - Double Ampersand)
			//Returns true if all operands are true
			let allRequirementsMet = isLegalAge && isRegistered;
			console.log('Result of Logical AND Operator: ' + allRequirementsMet);

		//Logical OR Operator (Double || - Double Pipe)
			// returns true if one of the operands are true
			let someRequirementsMet = isLegalAge || isRegistered;
			console.log('Result of Logical OR Operator: ' + someRequirementsMet);

		//Logical NOT Operator(! - Exclamation Point)
			// Returns the opposite value
			let someRequirementsNotMet = !isRegistered;
			console.log('Results of logical NOT Operator: ' + someRequirementsNotMet );























